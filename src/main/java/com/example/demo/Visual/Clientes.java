package com.example.demo.Visual;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Clientes {
    private String nombre;
    private String Apellido;
    private String Direccion;
    private String Correo;
    private int Telefono;
    private int Edad;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    public Clientes() {
    }

    public Clientes(String nombre, String apellido, String direccion, String correo, int telefono, int edad) {
        this.nombre = nombre;
        Apellido = apellido;
        Direccion = direccion;
        Correo = correo;
        Telefono = telefono;
        Edad = edad;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return Apellido;
    }

    public void setApellido(String apellido) {
        Apellido = apellido;
    }

    public String getDireccion() {
        return Direccion;
    }

    public void setDireccion(String direccion) {
        Direccion = direccion;
    }

    public String getCorreo() {
        return Correo;
    }

    public void setCorreo(String correo) {
        Correo = correo;
    }

    public int getTelefono() {
        return Telefono;
    }

    public void setTelefono(int telefono) {
        Telefono = telefono;
    }

    public int getEdad() {
        return Edad;
    }

    public void setEdad(int edad) {
        Edad = edad;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}

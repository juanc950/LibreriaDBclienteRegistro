package com.example.demo.Visual;

import com.vaadin.server.VaadinRequest;
import com.vaadin.spring.annotation.SpringUI;
import com.vaadin.ui.*;
import org.springframework.beans.factory.annotation.Autowired;

@SpringUI
public class App extends UI {

    @Autowired
    MyRepository clienteRepository;

    @Override

    protected void init(VaadinRequest vaadinRequest) {
        VerticalLayout layout = new VerticalLayout();
        HorizontalLayout hlayout = new HorizontalLayout();
        TextField nombre = new TextField("Nombre");
        TextField apellido = new TextField("Apellido");
        TextField direccion = new TextField("Direccion");
        TextField correo = new TextField("Correo");
        TextField telefono = new TextField("Telefono");
        TextField edad = new TextField("Edad");

        Grid<Clientes> grid = new Grid<>();
        grid.addColumn(Clientes::getId).setCaption("Id");
        grid.addColumn(Clientes::getNombre).setCaption("Nombre");
        grid.addColumn(Clientes::getApellido).setCaption("Apellido");
        grid.addColumn(Clientes::getDireccion).setCaption("Direccion");
        grid.addColumn(Clientes::getCorreo).setCaption("Correo");
        grid.addColumn(Clientes::getTelefono).setCaption("Telefono");
        grid.addColumn(Clientes::getEdad).setCaption("Edad");

        Button add = new Button("Adicionar");
        add.addClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent clickEvent) {
                Clientes e = new Clientes();
                e.setNombre(nombre.getValue());
                e.setApellido(apellido.getValue());
                e.setDireccion(direccion.getValue());
                e.setCorreo(correo.getValue());
                e.setTelefono(Integer.parseInt(telefono.getValue()));
                e.setEdad(Integer.parseInt(edad.getValue()));
                clienteRepository.save(e);
                grid.setItems(clienteRepository.findAll());

                nombre.clear();
                apellido.clear();
                direccion.clear();
                correo.clear();
                telefono.clear();
                edad.clear();
            }
        });
        hlayout.addComponents(nombre,apellido,direccion,correo,telefono, edad, add);
        hlayout.setComponentAlignment(add, Alignment.BOTTOM_RIGHT);
        layout.addComponent(hlayout);
        layout.addComponent(grid);
        setContent(layout);
    }
}
